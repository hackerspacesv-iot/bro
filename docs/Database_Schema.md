# Database Schema for BRO

BRO is a small C daemon in charge of listening MQTT events
and evaluate the defined rules (essentially expressions)
stored in a small Sqlite3 database.

This database schema pretends to be abstract enought to 
support not only different expression languages but also
to allow to have virtual/external nodes and rules.

## Database Entities
![BRO Database Schema](./BRO_DB_Schema.xml.png "BRO Database Schema")

Note: Database schema drawn with draw.io

### Table nodes
This table stores the MAC address of the node in the local
IoT network and describes it capabilities.

### Table node_topics
This table stores the available topics on wich BRO is going
to listen for new data. Also exposes the publish topics
were the leaf node is subscribed to receive data.

### Table rules
This table contains the expressions to be evaluated each
time the node publish something in a topic.

### Table operands
This table exists to facilitate the lookup of operands and
rules each time a new event is received.
