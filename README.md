# Border Router Orchestrator (BRO)

Border Router Orchestrator (BRO) - Is a daemon 
running in an IoT border gateway to "orchestrate" 
all the actions to be executed locally. This piece 
of software essentially acts as the "fog-computing" 
component of an IoT implementation. 

## Documentation
The following documentation is available:
* [Database Schema](./docs/Database_Schema.md)
